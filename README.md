### Documentation for Note Management System

### Backend: Spring Boot API


---
#### API Endpoints

| Method | Endpoint          | Description                       | Body                        |
|--------|-------------------|-----------------------------------|-----------------------------|
| GET    | /api/notes        | Get all notes                     | None                        |
| POST   | /api/notes        | Create a new note                 | { "title": "", "content": "" } |
| PUT    | /api/notes/{id}   | Update an existing note by ID     | { "title": "", "content": "" } |
| DELETE | /api/notes/{id}   | Delete a note by ID               | None                        |

---